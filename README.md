<p align="center">
  <a href="https://horizontheme.com/">
    <img alt="plex criterion project" src="https://i.imgur.com/oUILCGy.png" height="200">
  </a>
</p>

<p align="center">
<a href="https://github.com/tylernguyen/plex-criterion-project/issues">
   <img src="https://img.shields.io/gitlab/issues/tylernguyen/plex-criterion-project"/> </a>
<a href="https://github.com/tylernguyen/plex-criterion-project/commits/main"> 
   <img src="https://img.shields.io/gitlab/last-commit/tylernguyen/plex-criterion-project"/> </a>
</p>


<p align="center">
<a href="https://discord.gg/H6P7qwJykp"> 
   <img src="https://img.shields.io/discord/722512427333451969?color=%237289da&label=Discord&logo=Discord&logoColor=%237289da&style=for-the-badge"/> </a>
</p>

# Welcome to the Plex Criterion Project!
The Plex Criterion Project (PCP) was started by @crocs (crocs#2608 on Discord) on June 20, 2020.

## Mission Statement

per `@crocs`'s original statement,

We are a group of graphic designers and movie lovers that are taking the mission of resizing the Criterion Collection's poster, so that they are compatible with Plex and other self-hosted media servers.

# Using the Project

per its title, the project is geared towards Plex users and is intended to be used in conjunction with [meisnate12/Plex-Meta-Manager](https://github.com/meisnate12/Plex-Meta-Manager).

1. Editing pmm's `config.yml`, make sure:
    - Configure Plex connection and authentication
    - Configure a movie library, ie, `movies.yml`
    - Configure an assets directory (this is where our posters live by default)
    - `asset_folders` is set to `false`
2. Editing `movies.yml`
```
collections:
  The Criterion Collection:
    imdb_list: https://www.imdb.com/list/ls090936109
    item_assets: true
    sync_mode: append
    collection_mode: default
    collection_order: custom
```

# Getting Started as a Contributor

## Organizing, Review, and Proofchecking

1. Set a custom watch alert for `Issues`. You should now be notified when there are activies, be it new cover uploads or changes to existing covers.
2. Actively participate in the issue's comments to improve the design and asthetic of the poster.

## Designer

- Refer to the `template` folder for design assets.
- The primary goal is to have a uniform asthetic in line with the default Criteiron collection covers.

- New
    1. Create a GitHub issue with the `New` template.
    2. Upload your designed poster here, you should also upload the ziped `.psd` file for easier edits.
    3. Wait for maintainers, reviewers, and proof checkers to review your upload.
    4. A mod will manually approve your poster once it's cleared by reviews. By default, the poster will be accepted after 1 week if there are no review comments.
    5. Create a pull request with the asset.

- Existing
    1. Refer to the existing GitHub issue, create one if non-existent.
    2. Any proposed alterations and changes should be discussed in the existing GitHub issue. A mock-up of the proposed change would also be useful during this process.
    3. If change is approved, create a pull request.

**Things to keep an eye out out for:**

1. Check the date on the side of the poster, sometimes this can be forgotten about.
2. Small things like opacity of the logo and the spine are sometimes overlooked. 
3. Size and placement of the logo and spine.